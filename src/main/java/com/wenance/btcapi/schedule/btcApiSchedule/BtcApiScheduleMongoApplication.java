package com.wenance.btcapi.schedule.btcApiSchedule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan(basePackages = "com.wenance.btcapi.schedule.btcApiSchedule")
public class BtcApiScheduleMongoApplication {
	 
	public static void main(String[] args) {
		SpringApplication.run(BtcApiScheduleMongoApplication.class, args);
	}

}
