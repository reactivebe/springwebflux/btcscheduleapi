package com.wenance.btcapi.schedule.btcApiSchedule.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection="btc")
public class Btc implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2414565539795606177L;

	
	@Id
	private String id;
	
	private Float lprice;
	
	private String curr1;
	
	private String curr2;
	
	private LocalDateTime createDateTime;

}
