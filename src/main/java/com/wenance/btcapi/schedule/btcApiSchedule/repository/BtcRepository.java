package com.wenance.btcapi.schedule.btcApiSchedule.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.wenance.btcapi.schedule.btcApiSchedule.model.Btc;

@Repository
public interface BtcRepository extends MongoRepository<Btc, String> {

}
