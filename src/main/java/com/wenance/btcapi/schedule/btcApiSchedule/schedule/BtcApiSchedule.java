package com.wenance.btcapi.schedule.btcApiSchedule.schedule;

import java.time.LocalDateTime;

import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wenance.btcapi.schedule.btcApiSchedule.model.Btc;
import com.wenance.btcapi.schedule.btcApiSchedule.repository.BtcRepository;

@Component
public class BtcApiSchedule {
	 
	public BtcApiSchedule(BtcRepository btcRepository) {
		this.btcRepository = btcRepository;
	}
	
	private BtcRepository btcRepository;
	
	@Value("${btc.clientapi.url}")
	private String btcUrl;
	
	/**
	 * The next task won't be invoked until the previous one is done. 
	 * @throws JsonProcessingException 
	 * @throws JsonMappingException 
	 * 
	 */
	@Scheduled(fixedRate = 5000)
	public void saveBtcValue() throws JsonMappingException, JsonProcessingException {
		ResteasyClient clientBtc = new ResteasyClientBuilder().build();
        ResteasyWebTarget btcRequest = clientBtc.target(btcUrl);
        Response response = btcRequest.request().get();
        String resValue = response.readEntity(String.class);
        response.close();  // You should close connections!
        ObjectMapper objectMapper = new ObjectMapper();
        Btc btc = objectMapper.readValue(resValue, Btc.class);
        btc.setCreateDateTime(LocalDateTime.now());
        Btc savedBtc = btcRepository.save(btc);
        System.out.println(savedBtc);
	}

}
