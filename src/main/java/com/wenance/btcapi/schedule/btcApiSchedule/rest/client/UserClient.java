package com.wenance.btcapi.schedule.btcApiSchedule.rest.client;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;

import com.wenance.btcapi.schedule.btcApiSchedule.model.Btc;


@Path("/api/last_price/BTC/USD")
public interface UserClient {

    @GetMapping()
    @Produces(MediaType.APPLICATION_JSON_VALUE)
    Btc getBtcValue();

}
